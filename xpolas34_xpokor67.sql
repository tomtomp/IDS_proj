DROP TABLE pp_loan;
DROP TABLE pp_user;
DROP TABLE pp_teaser_diff;
DROP TABLE pp_difficulty;
DROP TABLE pp_solution;
DROP TABLE pp_puzzle;
DROP TABLE pp_riddle;
DROP TABLE pp_teaser;
DROP MATERIALIZED VIEW my_loans;

DROP SEQUENCE pp_user_seq;
DROP SEQUENCE pp_teaser_seq;
DROP SEQUENCE pp_solution_seq;
DROP SEQUENCE pp_difficulty_seq;

CREATE SEQUENCE pp_user_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_teaser_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_solution_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_difficulty_seq
START WITH 1
INCREMENT BY 1;

CREATE TABLE pp_user
(
  id int PRIMARY KEY,
  name varchar(30), 
  surname varchar(30), 
  role varchar(10), 
  city varchar(15), 
  street varchar(15), 
  country varchar(20), 
  zip varchar(10),
  lupdate date DEFAULT sysdate
);

CREATE OR REPLACE TRIGGER  pp_user_id_ai
  before insert on pp_user              
  for each row  
begin   
  if :new.id is null then 
    :new.id := pp_user_seq.nextval; 
  end if; 
end; 
/

CREATE TABLE pp_teaser
(
  id int DEFAULT pp_teaser_seq.NEXTVAL PRIMARY KEY, 
  name varchar(30), 
  country varchar(20), 
  type varchar(10) CHECK(type IN ('puzzle', 'riddle'))
);

CREATE TABLE pp_puzzle
(
  id int NOT NULL PRIMARY KEY,
  author_name varchar(30), 
  author_surname varchar(30), 
  theme varchar (20), 
  pieces int, 
  FOREIGN KEY (id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE
);

CREATE TABLE pp_riddle
(
  id int NOT NULL PRIMARY KEY,
  language varchar(30), 
  author_name varchar(30), 
  author_surname varchar(30),
  best_time int, 
  FOREIGN KEY (id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE
);

CREATE TABLE pp_loan
(
  pp_user_id int NOT NULL, 
  pp_teaser_id int NOT NULL, 
  l_from date, 
  l_to date, 
  res_from date, 
  res_to date, 
  success number(1), 
  rating int, 
  FOREIGN KEY (pp_user_id) REFERENCES pp_user(id), 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id), 
  PRIMARY KEY (pp_user_id, pp_teaser_id, l_from),
  CHECK (rating >= 0 AND rating <= 100)
);

CREATE TABLE pp_solution
(
  id int DEFAULT pp_solution_seq.NEXTVAL, 
  pp_teaser_id int NOT NULL, 
  solution clob, 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE, 
  PRIMARY KEY (pp_teaser_id, id)
);

CREATE TABLE pp_difficulty
(
  id int DEFAULT pp_difficulty_seq.NEXTVAL PRIMARY KEY, 
  value varchar(25)
);


CREATE TABLE pp_teaser_diff
(
  pp_difficulty_id int NOT NULL, 
  pp_teaser_id int NOT NULL, 
  value int, 
  FOREIGN KEY (pp_difficulty_id) REFERENCES pp_difficulty(id) ON DELETE CASCADE, 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id) ON DELETE CASCADE, 
  PRIMARY KEY (pp_difficulty_id, pp_teaser_id),
  CHECK (value >= 0 AND value <= 10)
);


INSERT INTO pp_user (name, surname, role, city)
  VALUES ('Tomáš', 'Polášek', 'admin', 'Rouchovany');
  
INSERT INTO pp_user (name, surname, role, city)
  VALUES ('Jan', 'Pokorný', 'guest', 'Olomouc');
  
INSERT INTO pp_user (name, surname, role, city)
  VALUES ('Petr', 'Jan', 'guest', 'Aš');
  
INSERT INTO pp_difficulty
  VALUES (DEFAULT, 'prostorová');

INSERT INTO pp_difficulty
  VALUES (DEFAULT, 'logická');

INSERT INTO pp_difficulty
  VALUES (DEFAULT, 'časová');
  
INSERT INTO pp_teaser (name, country)
  VALUES ('Rubikova kostka', 'Hungary');
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'prostorová'), 
          (SELECT id FROM pp_teaser WHERE name = 'Rubikova kostka'), 
          6);
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'logická'), 
          (SELECT id FROM pp_teaser WHERE name = 'Rubikova kostka'), 
          4);
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'časová'), 
          (SELECT id FROM pp_teaser WHERE name = 'Rubikova kostka'), 
          5);
  
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Mona Lisa', 'Germany', 'puzzle');
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'prostorová'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa'), 
          3);
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'logická'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa'), 
          0);
INSERT INTO pp_teaser_diff (pp_difficulty_id, pp_teaser_id, value) 
  VALUES ((SELECT id FROM pp_difficulty WHERE value = 'časová'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa'), 
          9);
  
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Mona Lisa2', 'Germany', 'puzzle');
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Mona Lisa3', 'Germany', 'puzzle');
INSERT INTO pp_puzzle (id, author_name, author_surname, theme, pieces)
  VALUES (pp_teaser_seq.currval, 'Ja', 'On' , 'Portrait', '1373');
INSERT INTO pp_solution (pp_teaser_id, solution)
  VALUES (pp_teaser_seq.currval, 'Poskládat dílky k sobě.');
  
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Počet slonů', 'France', 'riddle');
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Počet slonů2', 'France', 'riddle');
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Počet slonů3', 'France', 'riddle');
INSERT INTO pp_riddle (id, language, author_name, author_surname, best_time)
  VALUES (pp_teaser_seq.currval, 'French', 'Jan', 'Pokorný', 24);
INSERT INTO pp_solution (pp_teaser_id, solution)
  VALUES (pp_teaser_seq.currval, 'Uhádnout to.');

INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Tomáš'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa'), 
          TO_DATE('12.06.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.07.2016', 'DD.MM.YYYY'), 
          null, null, 1, 45);
INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Tomáš'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa2'), 
          TO_DATE('12.06.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.07.2016', 'DD.MM.YYYY'), 
          null, null, 0, 30);
          
INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Jan'), 
          (SELECT id FROM pp_teaser WHERE name = 'Počet slonů'), 
          TO_DATE('12.06.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.07.2016', 'DD.MM.YYYY'), 
          null, null, 1, 25);
          
INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Jan'), 
          (SELECT id FROM pp_teaser WHERE name = 'Počet slonů2'), 
          TO_DATE('12.06.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.07.2016', 'DD.MM.YYYY'), 
          null, null, 0, 55);

INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Jan'), 
          (SELECT id FROM pp_teaser WHERE name = 'Počet slonů2'), 
          TO_DATE('12.08.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.09.2016', 'DD.MM.YYYY'), 
          null, null, 0, 55);
          
COMMIT;

/* Přidaní rodného číslo + validace dělitelnosti 11*/
ALTER TABLE pp_user ADD rc int UNIQUE;
ALTER TABLE pp_user ADD CONSTRAINT chk_rc CHECK(MOD(rc,11)=0);

UPDATE pp_user SET rc=9409055754 WHERE name='Jan' and surname='Pokorný';/* chyba */
UPDATE pp_user SET rc=9409055755 WHERE name='Jan' and surname='Pokorný';

/* Spojení dvou tabulek */
/* Kolik kdo provedl výpůjček.*/
SELECT U.name, COUNT(*) Pocet
  FROM PP_USER U INNER JOIN PP_LOAN L ON U.id = L.pp_user_id
  GROUP BY U.name
  ORDER BY Pocet DESC;
  
  
/* Vypiš jména uživatelů, kteří si půjčili jenom puzzle, nebo nic. */
SELECT PP_USER.name FROM PP_USER WHERE ID NOT IN (
	SELECT PP_USER.ID FROM PP_USER 
	LEFT JOIN PP_LOAN ON PP_USER.ID = PP_LOAN.PP_USER_ID 
	WHERE PP_TEASER_ID in (
		SELECT PP_TEASER.ID FROM PP_TEASER WHERE TYPE != 'puzzle'
	)
);

/* Spojení tří tabulek */
/* Hádanky, které mají časovou složitost alespoň 6. */
SELECT T.name
  FROM PP_TEASER T INNER JOIN PP_TEASER_DIFF TD ON T.id = TD.pp_teaser_id
        INNER JOIN PP_DIFFICULTY D ON TD.pp_difficulty_id = D.id
  WHERE TD.value >= 6 AND D.value = 'časová';
  
/* Kdo si co pujcil. */
SELECT PP_USER.name, PP_TEASER.name
  FROM PP_USER INNER JOIN PP_LOAN ON PP_USER.id = PP_LOAN.pp_user_id LEFT JOIN 
    PP_TEASER ON PP_LOAN.pp_teaser_id = PP_TEASER.id;

/* Vypiš jména uživatelů, kteří si půjčili jenom puzzle. */
SELECT U.name
  FROM PP_USER U
  WHERE U.id not in (
    SELECT U.id
    FROM PP_USER U LEFT JOIN PP_LOAN L ON U.id = L.pp_user_id 
                   LEFT JOIN PP_TEASER T ON T.id = L.pp_teaser_id
    WHERE T.type != 'puzzle' or T.type is NULL);

/* Group by a agregační funkce */
/* Kolik unikátních věcí si kdo půjčil. */
SELECT U.name, COUNT(*) Pocet
  FROM PP_USER U INNER JOIN (
    SELECT DISTINCT L.pp_user_id, L.pp_teaser_id
      FROM PP_LOAN L
  ) L ON U.id = L.pp_user_id
  GROUP BY U.name
  ORDER BY Pocet DESC;
  
/* Hádanky, které byly vždy vyřešeny. */
SELECT T.name
  FROM PP_TEASER T INNER JOIN PP_LOAN L ON T.ID = L.pp_teaser_id
  WHERE T.type = 'riddle'
  GROUP BY T.name
  HAVING MIN(L.success) = 1;


/* Predikát EXISTS */
/* Hádanky, které, pokud byly někdy půjčeny, pak byly po každé vyřešeny. */
SELECT T.name FROM pp_teaser T WHERE NOT EXISTS (
  SELECT * FROM pp_loan L 
    WHERE L.success = 0 AND L.pp_teaser_id = T.id
) AND type = 'riddle';

/* Predikát IN */
/* Vypiš jména uživatelů, kteří si půjčili jenom puzzle, nebo nic. */
SELECT PP_USER.name FROM PP_USER WHERE ID NOT IN (
	SELECT PP_USER.ID FROM PP_USER 
	LEFT JOIN PP_LOAN ON PP_USER.ID = PP_LOAN.PP_USER_ID 
	WHERE PP_TEASER_ID in (
		SELECT PP_TEASER.ID FROM PP_TEASER WHERE TYPE != 'puzzle'
	)
);

/* 
Netrivilání trigger pro last update informaci 
Druhý na AI ID je hned za vytvořením tabulky pp_user
*/
CREATE OR REPLACE TRIGGER  pp_user_last_update
  before update on pp_user              
  for each row  
begin   
    :new.lupdate := sysdate; 
end; 
/
/* Explain plan */
EXPLAIN PLAN FOR
  SELECT pp_difficulty.value, AVG(pp_teaser_diff.value) prumer
  FROM pp_difficulty JOIN pp_teaser_diff on pp_difficulty.id = pp_teaser_diff.pp_teaser_id
  group by pp_difficulty.value ORDER BY pp_difficulty.value;
    
SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());
     
CREATE  INDEX "i_nazev" ON "PP_DIFFICULTY" ('value');

EXPLAIN PLAN FOR
   SELECT pp_difficulty.value, AVG(pp_teaser_diff.value) prumer
  FROM pp_difficulty JOIN pp_teaser_diff on pp_difficulty.id = pp_teaser_diff.pp_teaser_id
  group by pp_difficulty.value ORDER BY pp_difficulty.value;
    
SELECT PLAN_TABLE_OUTPUT FROM TABLE(DBMS_XPLAN.DISPLAY());

/* Přístupová práva */
GRANT SELECT ON pp_teaser TO xpolas34;
GRANT SELECT ON pp_puzzle TO xpolas34;
GRANT SELECT ON pp_riddle TO xpolas34;
GRANT SELECT ON pp_teaser_diff TO xpolas34;
GRANT SELECT ON pp_difficulty TO xpolas34;


/* Pohled pro uživatele jaký hlavolam si kdy půjčil. Uživalel jinak k těmto údajům nemá přístup*/ 
CREATE MATERIALIZED VIEW my_loans
  refresh with primary key
  AS
  SELECT PP_TEASER.*,PP_LOAN.*
  FROM PP_USER INNER JOIN PP_LOAN ON PP_USER.id = PP_LOAN.pp_user_id LEFT JOIN 
    PP_TEASER ON PP_LOAN.pp_teaser_id = PP_TEASER.id
  WHERE PP_USER.id = 2;

GRANT ALL ON my_loans TO xpolas34;

/* Demo mat. pohledu. Tabulka mat. pohledu zůstane stejná*/
SELECT * FROM my_loans;

INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Jan'), 
          (SELECT id FROM pp_teaser WHERE name = 'Počet slonů'), 
          TO_DATE('14.06.2017', 'DD.MM.YYYY'), 
          TO_DATE('18.07.2018', 'DD.MM.YYYY'), 
          null, null, 1, 25);

SELECT * FROM my_loans;

/* Procedura pro vytvoření puzzle hádanky, udělá práci ve dvou tabulkách na jendou */

CREATE OR REPLACE PROCEDURE insertPuzzle(name in varchar, country in varchar, author_name in varchar,
author_surname in varchar, theme in varchar, pieces in number) IS
BEGIN
  INSERT INTO pp_teaser (name, country, type)
    VALUES (name, country, 'puzzle');
  INSERT INTO pp_puzzle (id, author_name, author_surname, theme, pieces)
    VALUES (pp_teaser_seq.currval, author_name, author_surname , theme, pieces);
END;
/

BEGIN
  insertPuzzle('Nový puzzle','Česká republika', 'Jan', 'Pokorný', 'Krajina', 350);
END;
/

/* Procedůra pro udělení bodů uživateli na základě jeho úspěšnosti */

ALTER TABLE pp_user ADD points NUMBER;

CREATE OR REPLACE PROCEDURE points(pId in number) is
CURSOR points_cursor(pId NUMBER) IS
  SELECT success FROM pp_loan WHERE pp_user_id = pId;

e exception;  
radek points_cursor%ROWTYPE;
new_points NUMBER(10,0) := 0;
BEGIN
   OPEN points_cursor(pId);
   FETCH points_cursor INTO radek;
   IF points_cursor%found = FALSE THEN
    BEGIN
      raise e;
    END;
  END IF;
   WHILE points_cursor%found LOOP
    IF radek.success = 0 THEN
      BEGIN    
      new_points := new_points-3;
      END;
    ELSE
       new_points := new_points+5;
    END IF;
    FETCH points_cursor INTO radek;
   END LOOP;
   CLOSE points_cursor;
    dbms_output.put_line(new_points);
    UPDATE pp_user SET points = new_points WHERE id = pId;
  exception
    when e then
    dbms_output.put_line('No loans find');
END;
/

/* Před udělením bodů */
SELECT points FROM PP_USER WHERE id = 2;

BEGIN
  points(2);
END;
/

/* Po udělení bodů */
SELECT points FROM PP_USER WHERE id = 2;

/* Vyjímka - zachycena */
BEGIN
  points(422222);
END;
/
