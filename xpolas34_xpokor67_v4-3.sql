DROP TABLE pp_loan;
DROP TABLE pp_user;
DROP TABLE pp_teaser_diff;
DROP TABLE pp_difficulty;
DROP TABLE pp_solution;
DROP TABLE pp_puzzle;
DROP TABLE pp_riddle;
DROP TABLE pp_teaser;

DROP SEQUENCE pp_user_seq;
DROP SEQUENCE pp_teaser_seq;
DROP SEQUENCE pp_solution_seq;
DROP SEQUENCE pp_difficulty_seq;

CREATE SEQUENCE pp_user_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_teaser_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_solution_seq
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE pp_difficulty_seq
START WITH 1
INCREMENT BY 1;

CREATE TABLE pp_user
(
  id int DEFAULT pp_user_seq.NEXTVAL PRIMARY KEY,
  name varchar(30), 
  surname varchar(30), 
  role varchar(10), 
  city varchar(15), 
  street varchar(15), 
  country varchar(20), 
  zip varchar(10)
);

CREATE TABLE pp_teaser
(
  id int DEFAULT pp_teaser_seq.NEXTVAL PRIMARY KEY, 
  name varchar(30), 
  country varchar(20), 
  type varchar(10) CHECK(type IN ('puzzle', 'riddle'))
);

CREATE TABLE pp_puzzle
(
  id int NOT NULL PRIMARY KEY,
  author_name varchar(30), 
  author_surname varchar(30), 
  theme varchar (20), 
  pieces int, 
  FOREIGN KEY (id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE
);

CREATE TABLE pp_riddle
(
  id int NOT NULL PRIMARY KEY,
  language varchar(30), 
  author_name varchar(30), 
  author_surname varchar(30),
  best_time int, 
  FOREIGN KEY (id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE
);

CREATE TABLE pp_loan
(
  pp_user_id int NOT NULL, 
  pp_teaser_id int NOT NULL, 
  l_from date, 
  l_to date, 
  res_from date, 
  res_to date, 
  success number(1), 
  rating int, 
  FOREIGN KEY (pp_user_id) REFERENCES pp_user(id), 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id), 
  PRIMARY KEY (pp_user_id, pp_teaser_id, l_from)
);

CREATE TABLE pp_solution
(
  id int DEFAULT pp_solution_seq.NEXTVAL, 
  pp_teaser_id int NOT NULL, 
  solution clob, 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id)
    ON DELETE CASCADE, 
  PRIMARY KEY (pp_teaser_id, id)
);

CREATE TABLE pp_difficulty
(
  id int DEFAULT pp_difficulty_seq.NEXTVAL PRIMARY KEY, 
  value varchar(25)
);


CREATE TABLE pp_teaser_diff
(
  pp_difficulty_id int NOT NULL, 
  pp_teaser_id int NOT NULL, 
  value int, 
  FOREIGN KEY (pp_difficulty_id) REFERENCES pp_difficulty(id), 
  FOREIGN KEY (pp_teaser_id) REFERENCES pp_teaser(id), 
  PRIMARY KEY (pp_difficulty_id, pp_teaser_id)
);


INSERT INTO pp_user (name, surname, role, city)
  VALUES ('Tomáš', 'Polášek', 'admin', 'Rouchovany');
  
INSERT INTO pp_user (name, surname, role, city)
  VALUES ('Jan', 'Pokorný', 'guest', 'Olomouc');
  
INSERT INTO pp_teaser (name, country)
  VALUES ('Rubikova kostka', 'Hungary');
  
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Mona Lisa', 'Germany', 'puzzle');
INSERT INTO pp_puzzle (id, author_name, author_surname, theme, pieces)
  VALUES (pp_teaser_seq.currval, 'Ja', 'On' , 'Portrait', '1373');
INSERT INTO pp_solution (pp_teaser_id, solution)
  VALUES (pp_teaser_seq.currval, 'Poskládat dílky k sobě.');
  
INSERT INTO pp_teaser (name, country, type)
  VALUES ('Počet slonů', 'France', 'riddle');
INSERT INTO pp_riddle (id, language, author_name, author_surname, best_time)
  VALUES (pp_teaser_seq.currval, 'French', 'Jan', 'Pokorný', 24);
INSERT INTO pp_solution (pp_teaser_id, solution)
  VALUES (pp_teaser_seq.currval, 'Uhádnout to.');

INSERT INTO pp_loan (pp_user_id, pp_teaser_id, l_from, l_to, res_from, res_to, 
  success, rating) 
  VALUES ((SELECT id FROM pp_user WHERE name = 'Tomáš'), 
          (SELECT id FROM pp_teaser WHERE name = 'Mona Lisa'), 
          TO_DATE('12.06.2016', 'DD.MM.YYYY'), 
          TO_DATE('16.07.2016', 'DD.MM.YYYY'), 
          null, null, 1, 55);
          
COMMIT;

/* Kdo si co pujcil */
SELECT PP_USER.name, PP_TEASER.name
  FROM PP_USER INNER JOIN PP_LOAN ON PP_USER.id = PP_LOAN.pp_user_id LEFT JOIN 
    PP_TEASER ON PP_LOAN.pp_teaser_id = PP_TEASER.id;

